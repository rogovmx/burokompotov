class PartitionsController < ApplicationController
  # GET /partitions
  # GET /partitions.json
  def index
    @partitions = Partition.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @partitions }
    end
  end

  # GET /partitions/1
  # GET /partitions/1.json
  def show
    @partition = Partition.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @partition }
    end
  end

  # GET /partitions/new
  # GET /partitions/new.json
  def new
    @partition = Partition.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @partition }
    end
  end

  # GET /partitions/1/edit
  def edit
    @partition = Partition.find(params[:id])
  end

  # POST /partitions
  # POST /partitions.json
  def create
    @partition = Partition.new(params[:partition])

    respond_to do |format|
      if @partition.save
        format.html { redirect_to @partition, notice: 'Partition was successfully created.' }
        format.json { render json: @partition, status: :created, location: @partition }
      else
        format.html { render action: "new" }
        format.json { render json: @partition.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /partitions/1
  # PUT /partitions/1.json
  def update
    @partition = Partition.find(params[:id])

    respond_to do |format|
      if @partition.update_attributes(params[:partition])
        format.html { redirect_to @partition, notice: 'Partition was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @partition.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /partitions/1
  # DELETE /partitions/1.json
  def destroy
    @partition = Partition.find(params[:id])
    @partition.destroy

    respond_to do |format|
      format.html { redirect_to partitions_url }
      format.json { head :no_content }
    end
  end
end
