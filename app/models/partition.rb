class Partition < ActiveRecord::Base
  attr_accessible :content, :foot, :head, :img, :is_close, :part, :pos, :title


  scope :visible, where("is_close != ?", true).order(:pos)
end
