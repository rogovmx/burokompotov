class CreatePartitions < ActiveRecord::Migration
  def change
    create_table :partitions do |t|
      t.string :title
      t.string :img
      t.text :head
      t.text :content
      t.text :foot
      t.string :part
      t.boolean :is_close
      t.integer :pos

      t.timestamps
    end
  end
end
